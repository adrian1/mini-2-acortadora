# Mini-2-Acortadora
## Autor: Adrián Montes Linares

Repositorio de plantilla para la minipráctica 2 "Web acortadora de URLs versión Django". Recuerda que puedes consultar su enunciado en la guía de estudio (programa) de la asignatura.

## SuperUsuario
-Nombre = admin
-Contraseña = admin

## Objetivo
El objetivo es realizar una Web acortadora con Django, el usuario rellenara un formulario con la url y la url corta que quiere asignar a esa url, si en el campo short del formulario se recibe vacio se le asignara una url corta que sera igual a un número que ira creciendo una unidad por cada url con url corta vacia

## Models.py
- Clase ShortUrl que contendra la url, la short url, las visitas a una url desde nuestra Web y la fecha en la que se creo en nuestra pagina web
```python
from django.db import models

# Create your models here.

# Class shorts_url
class ShortUrl (models.Model):
    # Define the attributes of the class
    # url
    url = models.CharField(max_length=200)
    # short_url
    short = models.CharField(max_length=10)
    # visits
    visits = models.IntegerField(default=0)
    # date
    date = models.DateTimeField(auto_now_add=True)

    # Define the string representation of the class
    def __str__(self):
        # I want the date appears like this: 2020-02-02 20:20:20
        return self.url + ' -> ' + self.short + ' (' + str(self.visits) + ')' + ' (' + str(self.date.strftime('%Y-%m-%d %H:%M:%S')) + ')'     
```

## Url.py (aplicación)
```python
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('<str:short>/', views.redirect),

]
```

## Views.py
- El manejo de las solicitudes, es decir, como manejar cuando llega un post del formulario y guardarlo la relacion url-short_url, y como manejar cuando nos pidan la redirección a una pagina web
```python
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from .models import ShortUrl
from django.views.decorators.csrf import csrf_exempt
import datetime
from django.template import loader

def ExistsShortUrl(short):
    return ShortUrl.objects.filter(short=short).exists()

def noExistsShortUrlAndNotEmpty(short):
    return not ExistsShortUrl(short) and not isEmptyShortUrl(short)

def getCountOfEmptyShortUrls():
    counter = 1
    for short_url in ShortUrl.objects.all():
        if short_url.short.isdigit():
            counter += 1
    return counter

def createShortUrl(new_url, new_short):
    short_url = ShortUrl(url=new_url, short=new_short, date=datetime.datetime.now())
    short_url.save()


def isEmptyShortUrl(short):
    return short == ''

def correctUrl(url):
    new_url = url
    if not (new_url.startswith('http://') or new_url.startswith('https://')):
        new_url = 'https://' + new_url
    return new_url


def manageNewShortUrl(new_url, new_short):
    # Correct the url
    new_url = correctUrl(new_url)
    # If the short url not exits, create a new one
    if noExistsShortUrlAndNotEmpty(new_short):
        # Create a new short url
        createShortUrl(new_url, new_short)

    elif isEmptyShortUrl(new_short):
        # Get the count of empty short urls
        counter = getCountOfEmptyShortUrls()
        # Create a new short url
        createShortUrl(new_url, counter)

def managePost(request):
    # Get the url from the form
    new_url = request.POST['url']
    # Get the short url from the form
    new_short = request.POST['short']
    # Check if the  url already exists, if not create the new short url
    if not ShortUrl.objects.filter(url=new_url).exists():
        manageNewShortUrl(new_url, new_short)

# Create your views here.
# csrf_exempt is used to avoid the CSRF token error, {% csrf_token %}
@csrf_exempt
def index(request):
    urls = ShortUrl.objects.all()
    template = loader.get_template('index.html')
    if request.method == 'POST':
        managePost(request)
    
    context = {
        'urls': urls
    }
    return HttpResponse(template.render(context, request))

@csrf_exempt
def redirect(request, short):
    # Try to redirect to the url if the short url exists in the database return a 404 error
    try:
        short_url = ShortUrl.objects.get(short=short)
        short_url.visits += 1
        short_url.save()
        return HttpResponseRedirect(short_url.url)
    except ShortUrl.DoesNotExist:
        # Use the 404.html template
        template = loader.get_template('404.html')
        context = {
            'short_url': short
        }
        return HttpResponseNotFound(template.render(context, request))
```
- Destacar HttpresponseRedirect para la redirección
```python
return HttpResponseRedirect(short_url.url)
```
- Destacar HttpResponseNotFound cuando no esta la short_url en la base de datos y no se puede hacer la redirección
```python
return HttpResponseNotFound(template.render(context, request))
```