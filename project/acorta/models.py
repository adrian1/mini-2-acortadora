from django.db import models

# Create your models here.

# Class shorts_url
class ShortUrl (models.Model):
    # Define the attributes of the class
    # url
    url = models.CharField(max_length=200)
    # short_url
    short = models.CharField(max_length=10)
    # visits
    visits = models.IntegerField(default=0)
    # date
    date = models.DateTimeField(auto_now_add=True)

    # Define the string representation of the class
    def __str__(self):
        # I want the date appears like this: 2020-02-02 20:20:20
        return self.url + ' -> ' + self.short + ' (' + str(self.visits) + ')' + ' (' + str(self.date.strftime('%Y-%m-%d %H:%M:%S')) + ')'