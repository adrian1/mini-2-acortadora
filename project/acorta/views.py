from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from .models import ShortUrl
from django.views.decorators.csrf import csrf_exempt
import datetime
from django.template import loader

def ExistsShortUrl(short):
    return ShortUrl.objects.filter(short=short).exists()

def noExistsShortUrlAndNotEmpty(short):
    return not ExistsShortUrl(short) and not isEmptyShortUrl(short)

def getCountOfEmptyShortUrls():
    counter = 1
    for short_url in ShortUrl.objects.all():
        if short_url.short.isdigit():
            counter += 1
    return counter

def createShortUrl(new_url, new_short):
    short_url = ShortUrl(url=new_url, short=new_short, date=datetime.datetime.now())
    short_url.save()


def isEmptyShortUrl(short):
    return short == ''

def correctUrl(url):
    new_url = url
    if not (new_url.startswith('http://') or new_url.startswith('https://')):
        new_url = 'https://' + new_url
    return new_url


def manageNewShortUrl(new_url, new_short):
    # Correct the url
    new_url = correctUrl(new_url)
    # If the short url not exits, create a new one
    if noExistsShortUrlAndNotEmpty(new_short):
        # Create a new short url
        createShortUrl(new_url, new_short)

    elif isEmptyShortUrl(new_short):
        # Get the count of empty short urls
        counter = getCountOfEmptyShortUrls()
        # Create a new short url
        createShortUrl(new_url, counter)

def managePost(request):
    # Get the url from the form
    new_url = request.POST['url']
    # Get the short url from the form
    new_short = request.POST['short']
    # Check if the  url already exists, if not create the new short url
    if not ShortUrl.objects.filter(url=new_url).exists():
        manageNewShortUrl(new_url, new_short)

# Create your views here.
# csrf_exempt is used to avoid the CSRF token error, {% csrf_token %}
@csrf_exempt
def index(request):
    urls = ShortUrl.objects.all()
    template = loader.get_template('index.html')
    if request.method == 'POST':
        managePost(request)
    
    context = {
        'urls': urls
    }
    return HttpResponse(template.render(context, request))

@csrf_exempt
def redirect(request, short):
    # Try to redirect to the url if the short url exists in the database return a 404 error
    try:
        short_url = ShortUrl.objects.get(short=short)
        short_url.visits += 1
        short_url.save()
        return HttpResponseRedirect(short_url.url)
    except ShortUrl.DoesNotExist:
        # Use the 404.html template
        template = loader.get_template('404.html')
        context = {
            'short_url': short
        }
        return HttpResponseNotFound(template.render(context, request))